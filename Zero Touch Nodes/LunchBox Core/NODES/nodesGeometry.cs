﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.DesignScript.Runtime;
using Autodesk.DesignScript.Geometry;

using ProvingGround.Dyn.Utils;
using ProvingGround.Tesselation;
using ProvingGround.Tesselation.CLASSES;

namespace Geometry
{
    /// <summary>
    /// Curve Manipulation
    /// </summary>
    public static class Curves
    {
        /// <summary>
        /// Gets start and end points and parameter values
        /// </summary>
        /// <param name="curve">Curve</param>
        /// <returns name="StartPoint">Start point.</returns>
        /// <returns name="EndPoint">Start point.</returns>
        /// <returns name="StartParameter">Start parameter.</returns>
        /// <returns name="EndParameter">End parameter.</returns>
        /// <search>lunchbox,curve,points,start,end,parameter</search>
        [MultiReturn(new[] { "StartPoint", "EndPoint", "StartParameter", "EndParameter" })]
        public static Dictionary<string, object> EndPoints(Curve curve)
        {
            Point m_start = curve.StartPoint;
            Point m_end = curve.EndPoint;

            double m_startparam = curve.StartParameter();
            double m_endparam = curve.EndParameter();

            return new Dictionary<string, object>
      {
        {"StartPoint", m_start},
        {"EndPoint", m_end},
        {"StarParameter", m_startparam},
        {"EndParameter", m_endparam},
      };
        }

        /// <summary>
        /// Divides a curve using a distance between segments
        /// </summary>
        /// <param name="curve">Curve to divide</param>
        /// <param name="Distance">Distance between curve segments</param>
        /// <returns name="Points">A list of points</returns>
        /// <returns name="Planes">A list of planes</returns>
        /// <returns name="Tangents">A list of tangent vectors</returns>
        /// <returns name="Distances">A list of distances</returns>
        /// <returns name="Parameters">A list of parameter values</returns>
        /// <search>lunchbox,curve,point,plane,divide,tangent</search>
        [MultiReturn(new[] { "Points", "Planes", "Tangents", "Distances", "Parameters" })]
        public static Dictionary<string, object> DivideCurveByDistance(Curve curve, double Distance)
        {
            List<Point> m_points = new List<Point>();
            List<Plane> m_planes = new List<Plane>();
            List<double> m_distances = new List<double>();
            List<double> m_parameters = new List<double>();
            List<Vector> m_tangents = new List<Vector>();

            int Number = Convert.ToInt16(curve.Length / Distance);

            for (int i = 0; i <= Number; i++)
            {
                double m_dist = Distance * i;
                if (m_dist <= curve.Length)
                {
                    Point m_pt = curve.PointAtChordLength(m_dist);
                    Plane m_pln = curve.PlaneAtSegmentLength(m_dist);
                    Vector m_tan = curve.TangentAtParameter(curve.ParameterAtPoint(m_pt));
                    double m_param = curve.ParameterAtChordLength(m_dist);

                    m_points.Add(m_pt);
                    m_planes.Add(m_pln);
                    m_tangents.Add(m_tan);
                    m_distances.Add(m_dist);
                    m_parameters.Add(m_param);
                }
            }

            return new Dictionary<string, object>
      {
        {"Points", m_points},
        {"Planes", m_planes},
        {"Tangents", m_tangents},
        {"Distances", m_distances},
        {"Parameters", m_parameters}
      };
        }

        /// <summary>
        /// Divides a curve evenly along the parameter space
        /// </summary>
        /// <param name="curve">Curve to divide</param>
        /// <param name="Number">Number of divisions</param>
        /// <returns name="Points">A list of points</returns>
        /// <returns name="Planes">A list of planes</returns>
        /// <returns name="Tangents">A list of tangent vectors</returns>
        /// <returns name="Parameters">A list of parameter values</returns>
        /// <search>lunchbox,curve,point,plane,divide,tangent</search>
        [MultiReturn(new[] { "Points", "Planes", "Tangents", "Parameters" })]
        public static Dictionary<string, object> DivideCurve(Curve curve, double Number)
        {
            List<Point> m_points = new List<Point>();
            List<Plane> m_planes = new List<Plane>();
            List<double> m_parameters = new List<double>();
            List<Vector> m_tangents = new List<Vector>();

            double m_p1 = curve.StartParameter();
            double m_p2 = curve.EndParameter();
            double m_range = m_p2 - m_p1;
            double m_step = m_range / Number;

            for (int i = 0; i <= Number; i++)
            {
                double m_parm = m_step * i;
                Point m_pt = curve.PointAtParameter(m_parm);
                Plane m_pln = curve.PlaneAtParameter(m_parm);
                Vector m_tan = curve.TangentAtParameter(m_parm);

                m_points.Add(m_pt);
                m_planes.Add(m_pln);
                m_tangents.Add(m_tan);
                m_parameters.Add(m_parm);
            }

            return new Dictionary<string, object>
      {
        {"Points", m_points},
        {"Planes", m_planes},
        {"Tangents", m_tangents},
        {"Parameters", m_parameters}
      };
        }

        /// <summary>
        /// Get segment and vertices of a polycurve
        /// </summary>
        /// <param name="polycurve">PolyCurve</param>
        /// <returns name="Segments">PolyCurve segments.</returns>
        /// <returns name="Points">Points at PolyCurve discontinuities.</returns>
        /// <search>lunchbox,polycurve</search>
        [MultiReturn(new[] { "Segments", "Points" })]
        public static Dictionary<string, object> DeconstructPolyCurve(PolyCurve polycurve)
        {
            Curve[] curves = polycurve.Curves();
            List<Point> points = new List<Point>();
            foreach (Curve c in curves)
            {
                points.Add(c.StartPoint);
            }

            return new Dictionary<string, object>
      {
        {"Segments", curves},
        {"Points", points}
      };
        }

        /// <summary>
        /// Find the 'Shortest Walk' within a curve network.  This node uses ported open source code by Giulio Piacentino of McNeel and Associates.
        /// </summary>
        /// <param name="CurveNetwork">A list of curve segments defining a network.</param>
        /// <param name="Lengths">A list of lengths for each curve segment. Length does not need to be "actual" if you want to weight the curves.</param>
        /// <param name="Paths">A list lines defining the start and end of the path.</param>
        /// <returns name="Shortest Walk">The shortest walk path.</returns>
        /// <returns name="Links">Resulting links.</returns>
        /// <returns name="Directions">Resulting directions.</returns>
        /// <returns name="Length">Resulting lengths.</returns>
        /// <search>lunchbox, curves, shortest walk, sort, path, distance</search>
        [MultiReturn(new[] { "Shortest Walk", "Links", "Direction", "Length" })]
        public static Dictionary<string, object> ShortestWalk(List<Curve> CurveNetwork, List<double> Lengths, List<Line> Paths)
        {

            // calcuate stuff here
            ShortestWalkUtils m_swu = new ShortestWalkUtils(CurveNetwork, Lengths, Paths);
            m_swu.SolveShortestWalk();

            List<Curve> m_resultingcurves = m_swu.ResultCurves;
            List<int[]> m_links = m_swu.ResultLinks;
            List<bool[]> m_directions = m_swu.ResultDirections;
            List<double> m_lengths = m_swu.ResultLengths;

            return new Dictionary<string, object>
      {
        {"Shortest Walk", m_resultingcurves},
        {"Links", m_links },
        {"Direction", m_directions},
        {"Lengths", m_lengths}
      };
        }

        ///// <summary>
        ///// Calculates isovists curve rays from a start point.
        ///// </summary>
        ///// <param name="Position">Plane to calculate the Isovist</param>
        ///// <param name="RayLength">Maximum Ray Length</param>
        ///// <param name="StartAngle">Limiting start angle</param>
        ///// <param name="EndAngle">Limiting end angle</param>
        ///// <param name="Boundaries">Boundary geometry (must be in plane)</param>
        ///// <returns>Isovist rays</returns>
        //////[MultiReturn(new[] { "Rays", "Longest Ray", "Shortest Ray" })]
        //////public static Dictionary<string, object> Isovist(Plane Position, double RayLength, double StartAngle, double EndAngle, List<Curve> Boundaries)
        //////{
        //////  List<Curve> m_rays = new List<Curve>();
        //////  Curve m_longestray = null;
        //////  Curve m_shortestray = null;

        //////  // calculate stuff here

        //////  return new Dictionary<string, object>
        //////  {
        //////    {"Rays", m_rays},
        //////    {"Longest Ray", m_longestray },
        //////    {"Shortest Ray", m_shortestray }
        //////  };
        //////}

    }

    /// <summary>
    /// Create and manipulate points
    /// </summary>
    public static class Points
    {
        /// <summary>
        /// Returns the X,Y, and Z coordinates of a point
        /// </summary>
        /// <param name="point">Point object</param>
        /// <returns path="X">X Coordinate</returns>
        /// <returns path="Y">Y Coordinate</returns>
        /// <returns path="Z">Z Coordinate</returns>
        /// <search>lunchbox,point,x,y,z</search>
        [MultiReturn(new[] { "X", "Y", "Z" })]
        public static Dictionary<string, object> DeconstructPoint(Point point)
        {
            return new Dictionary<string, object>
      {
        {"X", point.X},
        {"Y", point.Y},
        {"Z", point.Z}
      };
        }

        /// <summary>
        /// Create points from a list of 2 or 3 numbers
        /// </summary>
        /// <param name="Numbers">Number values (ordered in X,Y,Z)</param>
        /// <returns path="Point">Point.</returns>
        /// <returns>Points</returns>
        [MultiReturn(new[] { "Point" })]
        public static Dictionary<string, object> NumbersToPoint(List<double> Numbers)
        {
            Point pt = null;
            if (Numbers.Count >= 3)
            {
                pt = Point.ByCoordinates(Numbers[0], Numbers[1], Numbers[2]);
            }
            else if (Numbers.Count == 2)
            {
                pt = Point.ByCoordinates(Numbers[0], Numbers[1], 0);
            }
            else if (Numbers.Count == 1)
            {
                pt = Point.ByCoordinates(Numbers[0], 0, 0);
            }

            return new Dictionary<string, object>
      {
        {"Point", pt}
      };
        }

        /// <summary>
        /// Create numbers from points
        /// </summary>
        /// <param name="point">Point</param>
        /// <returns name="Numbers">Numbers (coordinate list)</returns>
        /// <search>lunchbox,point</search>
        [MultiReturn(new[] { "Numbers" })]
        public static Dictionary<string, object> PointToNumbers(Point point)
        {
            List<double> m_ptnum = new List<double>();
            m_ptnum.Add(point.X);
            m_ptnum.Add(point.Y);
            m_ptnum.Add(point.Z);

            return new Dictionary<string, object>
      {
        {"Numbers",m_ptnum}
      };
        }

        /// <summary>
        /// Converts a comma separated point string to a point object
        /// </summary>
        /// <param name="Text">Point string</param>
        /// <returns path="Point">Point.</returns>
        /// <search>lunchbox,point,string</search>
        [MultiReturn(new[] { "Point" })]
        public static Dictionary<string, object> StringToPoint(string Text)
        {
            string[] strarr = Text.Split(',');
            double x = Convert.ToDouble(strarr[0]);
            double y = Convert.ToDouble(strarr[1]);
            double z = Convert.ToDouble(strarr[2]);

            Point pt = Point.ByCoordinates(x, y, z);

            return new Dictionary<string, object>
      {
        {"Point", pt}
      };
        }

        /// <summary>
        /// Converts a Point to comma separted point string
        /// </summary>
        /// <param name="point">Point</param>
        /// <returns name="Text">Point string.</returns>
        /// <search>lunchbox,point,string</search>
        [MultiReturn(new[] { "Text" })]
        public static Dictionary<string, object> PointToString(Point point)
        {
            string pt = point.X + "," + point.Y + "," + point.Z;

            return new Dictionary<string, object>
      {
        {"Text", pt}
      };
        }

        /// <summary>
        /// Sort points along a curve
        /// </summary>
        /// <param name="Points">Points</param>
        /// <param name="Guide">Guide curve</param>
        /// <returns path="SortedPoints">Points sorted along a curve.</returns>
        /// <remarks path="Indices">Index map of sorted points.</remarks>
        /// <search>lunchbox,point,sort</search>
        [MultiReturn(new[] { "SortedPoints", "Indices" })]
        public static Dictionary<string, object> SortPointsAlongCurve(List<Point> Points, Curve Guide)
        {
            List<double> m_parameter1 = new List<double>();
            List<double> m_parameter2 = new List<double>();
            List<int> m_indices = new List<int>();

            int i = 0;
            foreach (Point pt in Points)
            {
                Point closest = Guide.ClosestPointTo(pt);
                double param = Guide.ParameterAtPoint(closest);
                m_parameter1.Add(param);
                m_parameter2.Add(param);
                m_indices.Add(i);
                i++;
            }

            Point[] m_ptarr = Points.ToArray();
            int[] m_ind = m_indices.ToArray();
            double[] m_key = m_parameter1.ToArray();
            double[] m_key2 = m_parameter2.ToArray();

            Array.Sort(m_key, m_ptarr);
            Array.Sort(m_key2, m_ind);

            return new Dictionary<string, object>
      {
        {"SortedPoints", m_ptarr},
        {"Indices", m_ind}
      };

        }

    }

    /// <summary>
    /// Surface Manipulation
    /// </summary>
    public static class Surfaces
    {
        /// <summary>
        /// Divides a surface using UV divisions
        /// </summary>
        /// <param name="surface">Surface</param>
        /// <param name="UDivision">U division</param>
        /// <param name="VDivision">V division</param>
        /// <returns name="Points">A list of points</returns>
        /// <returns name="Planes">A list of planes</returns>
        /// <returns name="Normals">A list of normal vectors.</returns>
        /// <returns name="Gaussian">A list of Gaussian curvature values</returns>
        /// <returns name="UParams">A list of U parameter values</returns>
        /// <returns name="VParams">A list of V parameter values</returns>
        /// <search>lunchbox,surface,point,plane,divide,normal,curvature</search>
        [MultiReturn(new[] { "Points", "Planes", "Normals", "Gaussian", "UParams", "VParams" })]
        public static Dictionary<string, object> DivideSurfaceUV(Surface surface, int UDivision, int VDivision)
        {
            double m_uStep = 1.0 / UDivision;
            double m_vStep = 1.0 / VDivision;

            List<List<Point>> m_points = new List<List<Point>>();
            List<List<Plane>> m_planes = new List<List<Plane>>();
            List<List<Vector>> m_normals = new List<List<Vector>>();
            List<List<double>> m_curvature = new List<List<double>>();
            List<List<double>> m_uparams = new List<List<double>>();
            List<List<double>> m_vparams = new List<List<double>>();

            for (int i = 0; i <= UDivision; i++)
            {
                List<Point> m_ptrow = new List<Point>();
                List<Plane> m_plnrow = new List<Plane>();
                List<Vector> m_normrow = new List<Vector>();
                List<double> m_curvrow = new List<double>();
                List<double> m_urow = new List<double>();
                List<double> m_vrow = new List<double>();
                for (int j = 0; j <= VDivision; j++)
                {
                    double u = m_uStep * i;
                    double v = m_vStep * j;

                    Point m_pt = surface.PointAtParameter(u, v);
                    Plane m_pl = surface.CoordinateSystemAtParameter(u, v).XYPlane;
                    Vector m_norm = surface.NormalAtParameter(u, v);
                    double m_crv = surface.GaussianCurvatureAtParameter(u, v);

                    m_ptrow.Add(m_pt);
                    m_plnrow.Add(m_pl);
                    m_normrow.Add(m_norm);
                    m_curvrow.Add(m_crv);
                    m_urow.Add(u);
                    m_vrow.Add(v);
                }
                m_points.Add(m_ptrow);
                m_planes.Add(m_plnrow);
                m_normals.Add(m_normrow);
                m_curvature.Add(m_curvrow);
                m_uparams.Add(m_urow);
                m_vparams.Add(m_vrow);
            }

            return new Dictionary<string, object>
              {
                {"Points", m_points},
                {"Planes", m_planes},
                {"Normals", m_normals},
                {"Gaussian", m_curvature},
                {"UParams", m_uparams},
                {"VParams", m_vparams}
              };
        }

        /// <summary>
        /// Deconstructs a surface into edge curves and points
        /// </summary>
        /// <param name="surface">Surface</param>
        /// <returns name="Edges">Edge curves.</returns>
        /// <returns name="Points">Corner points.</returns>
        [MultiReturn(new[] { "Edges", "Points" })]
        public static Dictionary<string, object> DeconstructSurface(Surface surface)
        {
            Curve[] m_perimeter = surface.PerimeterCurves();
            List<Point> m_points = new List<Point>();
            foreach (Curve c in m_perimeter)
            {
                m_points.Add(c.StartPoint);
            }

            return new Dictionary<string, object>
              {
                {"Edges", m_perimeter},
                {"Points", m_points}
              };
        }


        /// <summary>
        /// Rebuild the Surface into desired U and V space
        /// </summary>
        /// <param name="surface">Surface</param>
        /// <param name="u">Number of U spans after rebuilding</param>
        /// <param name="v">Number of V spans after rebuilding</param>
        /// <returns name="Rebuilt">Rebuilt surface</returns>
        [MultiReturn(new[] { "Rebuilt" })]
        public static Dictionary<string, object> RebuildSurface(Surface surface, int uDiv, int vDiv)
        {
            double m_uStep = 1.0 / uDiv;
            double m_vStep = 1.0 / vDiv;
            List<Curve> crvs = new List<Curve>();
            Surface s = null;
            for (int i = 0; i <= uDiv; i++)
            {
                List<Point> m_ptrow = new List<Point>();

                for (int j = 0; j <= vDiv; j++)
                {
                    double u = m_uStep * i;
                    double v = m_vStep * j;

                    Point m_pt = surface.PointAtParameter(u, v);

                    m_ptrow.Add(m_pt);
                }
                Curve crv = NurbsCurve.ByPoints(m_ptrow);
                crvs.Add(crv);
            }
            s = Surface.ByLoft(crvs);

            return new Dictionary<string, object>
              {
                {"Rebuilt", s},
              };
        }

        /// <summary>
        /// Transpose the UV space of the surface
        /// </summary>
        /// <param name="surface">Surface</param>
        /// <returns name="Transposed">Transposed Surface</returns>
        [MultiReturn(new[] { "Transposed" })]
        public static Dictionary<string, object> TransposeSurfaceUV(Surface surface)
        {
            NurbsSurface n = surface.ToNurbsSurface();
            Point[][] ctrlPts = n.ControlPoints();
            Double[][] weights = n.Weights();
            Double[] knotsU = n.UKnots();
            Double[] knotsV = n.VKnots();
            int degreeU = n.DegreeU;
            int degreeV = n.DegreeV;
            int uNumber = n.NumControlPointsU;
            int vNumber = n.NumControlPointsV;

            Point[][] transPts = new Point[vNumber][];
            double[][] transWeights = new double[vNumber][];
            for (int v = 0; v < vNumber; v++)
            {
                transPts[v] = new Point[uNumber];
                transWeights[v] = new double[uNumber];
                for (int u = 0; u < uNumber; u++)
                {
                    transPts[v][u] = ctrlPts[u][v];
                    transWeights[v][u] = weights[u][v];
                }

            }

            Surface s = NurbsSurface.ByControlPointsWeightsKnots(transPts, transWeights, knotsV, knotsU, degreeV, degreeU);

            return new Dictionary<string, object>
              {
                {"Transposed", s},
              };
        }

        /// <summary>
        /// Deconstruct a Surface as a NurbsSurface
        /// </summary>
        /// <param name="surface">Surface</param>
        /// <returns name="CtrlPoints">NurbsSurface control points</returns>
        /// <returns name="Weights">Control point weights</returns>
        /// <returns name="KnotsU">Knots in the U direction</returns>
        /// <returns name="KnotsV">Knots in the V direction</returns>
        /// <returns name="DegreeU">Degree in the U direction</returns>
        /// <returns name="DegreeU">Degree in the V direction</returns>
        /// <returns name="NumberU">Number of control points in the U direction</returns>
        /// <returns name="NumberV">Number of control points in the V direction</returns>
        [MultiReturn(new[] { "CtrlPoints", "Weights", "KnotsU", "KnotsV", "DegreeU", "DegreeV", "NumberU", "NumberV" })]
        public static Dictionary<string, object> DeconstructNurbsSurface(Surface surface)
        {
            NurbsSurface n = surface.ToNurbsSurface();
            Point[][] ctrlPts = n.ControlPoints();
            Double[][] weights = n.Weights();
            Double[] knotsU = n.UKnots();
            Double[] knotsV = n.VKnots();
            int degreeU = n.DegreeU;
            int degreeV = n.DegreeV;
            int uNumber = n.NumControlPointsU;
            int vNumber = n.NumControlPointsV;

            return new Dictionary<string, object>
              {
                {"CtrlPoints", ctrlPts},
                {"Weights", weights },
                {"KnotsU", knotsU },
                {"KnotsV", knotsV },
                {"DegreeU", degreeU },
                {"DegreeV", degreeV },
                {"NumberU", uNumber },
                {"NumberV", vNumber }
              };
        }


        /// <summary>
        /// Deconstructs a polysurface into faces, edge curves, and points
        /// </summary>
        /// <param name="polysurface">PolySurface</param>
        /// <returns name="Faces">Face surfaces.</returns>
        /// <returns name="Points">Corner points.</returns>
        [MultiReturn(new[] { "Faces", "Points" })]
        public static Dictionary<string, object> DeconstructPolySurface(PolySurface polysurface)
        {
            Surface[] m_faces = polysurface.Surfaces();
            List<List<Curve>> m_perimeter = new List<List<Curve>>();
            List<Point> m_points = new List<Point>();

            foreach (Surface s in polysurface.Surfaces())
            {
                foreach (Curve c in s.PerimeterCurves())
                {
                    m_points.Add(c.StartPoint);
                }
            }

            Point[] m_nodups = Point.PruneDuplicates(m_points);

            return new Dictionary<string, object>
      {
        {"Faces", m_faces},
        {"Points", m_nodups}
      };
        }
    }

    /// <summary>
    /// Solid forms
    /// </summary>
    public static class Solid
    {

    }

    /// <summary>
    /// Mesh objects
    /// </summary>
    public static class Meshes
    {
        /// <summary>
        /// Deconstructs a mesh into points, faces, and edges
        /// </summary>
        /// <param name="surface">Surface</param>
        /// <returns name="Vertices">Mesh vertices.</returns>
        /// <returns name="Edges">Mesh edge lines.</returns>
        /// <returns name="Faces">Mesh face definition.</returns>
        [MultiReturn(new[] { "Vertices", "Normals", "Edges", "Faces" })]
        public static Dictionary<string, object> DeconstructMesh(Mesh mesh)
        {
            Point[] m_verts = mesh.VertexPositions;
            Vector[] m_vecs = mesh.VertexNormals;
            IndexGroup[] m_index = mesh.FaceIndices;

            List<Line> m_edges = new List<Line>();
            for (int i = 0; i < m_index.Count(); i++)
            {
                List<Line> edgeCol = new List<Line>();
                IndexGroup group = m_index[i];
                uint count = group.Count;
                if (count == 3)
                {
                    Point a = m_verts[group.A];
                    Point b = m_verts[group.B];
                    Point c = m_verts[group.C];
                    edgeCol.Add(Line.ByStartPointEndPoint(a, b));
                    edgeCol.Add(Line.ByStartPointEndPoint(b, c));
                    edgeCol.Add(Line.ByStartPointEndPoint(c, a));
                }
                if (count == 4)
                {
                    Point a = m_verts[group.A];
                    Point b = m_verts[group.B];
                    Point c = m_verts[group.C];
                    Point d = m_verts[group.D];
                    edgeCol.Add(Line.ByStartPointEndPoint(a, b));
                    edgeCol.Add(Line.ByStartPointEndPoint(b, c));
                    edgeCol.Add(Line.ByStartPointEndPoint(c, d));
                    edgeCol.Add(Line.ByStartPointEndPoint(d, a));
                }

                // check if edge is already in the list
                foreach (Line ln in edgeCol)
                {
                    bool match = false;
                    string xyzStart1 = ln.StartPoint.X.ToString() + "," + ln.StartPoint.Y.ToString() + "," + ln.StartPoint.Z.ToString();
                    string xyzEnd1 = ln.EndPoint.X.ToString()+ "," + ln.EndPoint.Y.ToString() + "," + ln.EndPoint.Z.ToString();
                    foreach (Line master in m_edges)
                    {
                        string xyzStart2 = master.StartPoint.X.ToString() + "," + master.StartPoint.Y.ToString() + "," + master.StartPoint.Z.ToString();
                        string xyzEnd2 = master.EndPoint.X.ToString() + "," + master.EndPoint.Y.ToString() + "," + master.EndPoint.Z.ToString();

                        if (xyzStart1 == xyzStart2 && xyzEnd1 == xyzEnd2)
                        {
                            match = true;
                            break;
                        }
                        else if (xyzStart1 == xyzEnd2 && xyzEnd1 == xyzStart2)
                        {
                            match = true;
                            break;
                        }
                    }

                    // if no match found, add it to the list
                    if (match == false)
                    {
                        m_edges.Add(ln);
                    }
                }
            }

            return new Dictionary<string, object>
              {
                {"Vertices", m_verts},
                {"Normals", m_vecs},
                {"Edges", m_edges },
                {"Faces", m_index },
              };
        }
    }

    /// <summary>
    /// Intersection logic
    /// </summary>
    public static class Intersection
    {
        /// <summary>
        /// Find intersections from a list of geometry and a list of cutting geometry.
        /// </summary>
        /// <param name="geo">List of geometry</param>
        /// <param name="others">List of cutting geometry</param>
        /// <returns name="intersections">List of intersection geometry</returns>
        public static List<List<Autodesk.DesignScript.Geometry.Geometry>> FindIntersections(List<Autodesk.DesignScript.Geometry.Geometry> geo, List<Autodesk.DesignScript.Geometry.Geometry> others)
        {
            List<List<Autodesk.DesignScript.Geometry.Geometry>> m_intersections = new List<List<Autodesk.DesignScript.Geometry.Geometry>>();

            foreach (Autodesk.DesignScript.Geometry.Geometry g in geo)
            {
                List<Autodesk.DesignScript.Geometry.Geometry> intersections = new List<Autodesk.DesignScript.Geometry.Geometry>();
                foreach (Autodesk.DesignScript.Geometry.Geometry c in others)
                {
                    Autodesk.DesignScript.Geometry.Geometry[] interArr = g.Intersect(c);
                    if (interArr.Length > 0)
                    {
                        foreach (Autodesk.DesignScript.Geometry.Geometry i in interArr)
                        {
                            intersections.Add(i);
                        }
                    }
                }
                m_intersections.Add(intersections);
            }

            return m_intersections;
        }

        /// <summary>
        /// Find intersections from a list of geometry and a list of cutting geometry.
        /// </summary>
        /// <param name="origin">Origin point for the isovist</param>
        /// <param name="radius">Max radius of the rays</param>
        /// <param name="startAngle">Start angle for the ray cast</param>
        /// <param name="endAngle">End angle for the ray cast</param>
        /// <param name="rayCount">Number of rays to be cast</param>
        /// <param name="boundaries">A list of solid boundaries</param>
        /// <returns name="Rays">A list of rays</returns>
        /// <returns name="MinLength">Smallest ray length</returns>
        /// <returns name="MaxLength">Largest ray length</returns>
        /// <returns name="AvgLength">Average ray length</returns>
        [MultiReturn(new[] { "Rays", "MinLength", "MaxLength", "AvgLength" })]
        public static Dictionary<string, object> CreateIsovist(Point origin, double radius, double startAngle, double endAngle, int rayCount, List<Autodesk.DesignScript.Geometry.Geometry> boundaries)
        {

            Arc m_arc = Arc.ByCenterPointRadiusAngle(origin, radius, startAngle, endAngle, Vector.ZAxis());
            Curve[] m_divs = m_arc.DivideEqually(rayCount);
            List<Line> m_curves = new List<Line>();
            foreach (Curve c in m_divs)
            {
                Line ln = Line.ByStartPointEndPoint(origin, c.StartPoint);
                m_curves.Add(ln);
            }

            List<Line> m_rays = new List<Line>();
            List<double> m_lengths = new List<double>();

            foreach (Line l in m_curves)
            {
                List<Point> intersections = new List<Point>();
                List<double> distances = new List<double>();
                foreach (Autodesk.DesignScript.Geometry.Geometry c in boundaries)
                {
                    Autodesk.DesignScript.Geometry.Geometry[] interArr = l.Intersect(c);
                    if (interArr.Length > 0)
                    {
                        foreach (Autodesk.DesignScript.Geometry.Geometry i in interArr)
                        {
                            Curve intCrv = (Curve)i;
                            Point intPt = intCrv.StartPoint;
                            double intDist = origin.DistanceTo(intPt);
                            intersections.Add(intPt);
                            distances.Add(intDist);
                        }
                    }
                }

                if (intersections.Count > 0)
                {
                    double[] dists = distances.ToArray();
                    Point[] inters = intersections.ToArray();
                    Array.Sort(dists, inters);

                    Line ray = Line.ByStartPointEndPoint(origin, inters[0]);

                    m_rays.Add(ray);
                    m_lengths.Add(ray.Length);
                }
                else
                {
                    m_rays.Add(l);
                    m_lengths.Add(l.Length);
                }
            }

            Line[] m_rayArr;
            double[] m_lenArr;
            double m_maxLen = 0;
            double m_minLen = 0;
            double m_avgLen = 0;
            if (m_rays.Count > 0)
            {
                m_rayArr = m_rays.ToArray();
                m_lenArr = m_lengths.ToArray();
                Array.Sort(m_lenArr, m_rayArr);

                double total = 0;
                foreach (double l in m_lenArr)
                {
                    total = total + l;
                }
                m_minLen = m_lenArr[0];
                m_maxLen = m_lenArr[m_lenArr.Count() - 1];
                m_avgLen = total / (m_lenArr.Count());
            }


            return new Dictionary<string, object>
            {
                {"Rays", m_rays},
                {"MinLength", m_minLen},
                {"MaxLength", m_maxLen},
                {"AvgLength", m_avgLen},
            };
        }


        /// <summary>
        /// Advanced Geometry Splitting
        /// </summary>
        /// <param name="geometry">Geometry to Cut</param>
        /// <param name="cutting">List of Cutting Objects</param>
        /// <returns name="SplitGeometry">Split geometry.</returns>
        [MultiReturn(new[] { "SplitGeometry" })]
        public static Dictionary<string, object> GeometrySplit(Surface geometry, Autodesk.DesignScript.Geometry.Geometry[] cutting)
        {
            List<Autodesk.DesignScript.Geometry.Geometry> m_geolist = new List<Autodesk.DesignScript.Geometry.Geometry>();
            m_geolist.Add(geometry);
            Autodesk.DesignScript.Geometry.Geometry[] m_geoarr = m_geolist.ToArray();

            // Iterate through cutting objects
            for (int i = 0; i < cutting.Count(); i++)
            {
                Autodesk.DesignScript.Geometry.Geometry m_cut = cutting[i];
                List<Autodesk.DesignScript.Geometry.Geometry> m_storage = new List<Autodesk.DesignScript.Geometry.Geometry>();

                // iterate through geometry to cut
                for (int j = 0; j < m_geoarr.Count(); j++)
                {
                    Autodesk.DesignScript.Geometry.Geometry m_geo = m_geoarr[j];
                    // cut the geometry
                    try
                    {
                        Autodesk.DesignScript.Geometry.Geometry[] m_cutgeos = m_geo.Split(m_cut);
                        for (int k = 0; k < m_cutgeos.Count(); k++)
                        {
                            // add to list
                            Autodesk.DesignScript.Geometry.Geometry m_geoitem = m_cutgeos[k];
                            m_storage.Add(m_geoitem);
                        }
                    }
                    catch
                    {
                        m_storage.Add(m_geo);
                    }

                }

                // new geometry array for cutting
                m_geoarr = m_storage.ToArray();
            }

            return new Dictionary<string, object>
      {
        {"SplitGeometry", m_geoarr}
      };
        }
    }

    /// <summary>
    /// Paneling Systems
    /// </summary>
    public static class Panel
    {
        /// <summary>
        /// Create quad panels
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <returns name = "Panels">Panel surfaces</returns>
        /// <returns name = "Points">Panel points</returns>
        [MultiReturn(new[] { "Panels", "Points" })]
        public static Dictionary<string, object> PanelQuad(Surface srf, int u, int v)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Surface> m_panels = new List<Surface>();

            TesselationUtils tess = new TesselationUtils();
            List<clsQuad> m_quads = tess.PanelQuad(u, v);
            foreach (clsQuad q in m_quads)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));
                m_pts.Add(srf.PointAtParameter(q.D.X, q.D.Y));

                Surface panel = Surface.ByPerimeterPoints(m_pts);

                m_points.Add(m_pts);
                m_panels.Add(panel);
            }
            return new Dictionary<string, object>
            {
                {"Panels", m_panels},
                {"Points", m_points },
            };
        }

        /// <summary>
        /// Create staggered quad panels
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <returns name = "Panels">Panel surfaces</returns>
        /// <returns name = "Points">Panel points</returns>
        [MultiReturn(new[] { "Panels", "Points" })]
        public static Dictionary<string, object> PanelQuadStaggered(Surface srf, int u, int v)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Surface> m_panels = new List<Surface>();

            TesselationUtils tess = new TesselationUtils();
            List<clsQuad> m_quads = tess.PanelQuadStaggered(u, v);
            foreach (clsQuad q in m_quads)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));
                m_pts.Add(srf.PointAtParameter(q.D.X, q.D.Y));

                Surface panel = Surface.ByPerimeterPoints(m_pts);

                m_points.Add(m_pts);
                m_panels.Add(panel);
            }
            return new Dictionary<string, object>
            {
                {"Panels", m_panels},
                {"Points", m_points },
            };
        }

        /// <summary>
        /// Create randomized quad panels
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <param name="s">Random Seed</param>
        /// <returns name = "Panels">Panel surfaces</returns>
        /// <returns name = "Points">Panel points</returns>
        [MultiReturn(new[] { "Panels", "Points" })]
        public static Dictionary<string, object> PanelQuadRandom(Surface srf, int u, int v, int s)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Surface> m_panels = new List<Surface>();

            TesselationUtils tess = new TesselationUtils();
            List<clsQuadVar> m_quads = tess.RandomQuad(u, v, s);
            foreach (clsQuadVar q in m_quads)
            {
                if (q.RowCount > 1)
                {
                    List<Curve> crvs = new List<Curve>();
                    List<Point> dsPts = new List<Point>();
                    List<clsPoint> pts = q.Points;
                    int rowCount = q.RowCount;
                    IEnumerable<clsPoint> ptsA = pts.Take(rowCount);
                    IEnumerable<clsPoint> ptsB = pts.Skip(rowCount).Take(rowCount);

                    List<Point> dsPtsA = new List<Point>();
                    foreach (clsPoint p in ptsA)
                    {
                        Point dsPt = srf.PointAtParameter(p.X, p.Y);
                        dsPtsA.Add(dsPt);
                        dsPts.Add(dsPt);
                    }

                    List<Point> dsPtsB = new List<Point>();
                    foreach (clsPoint p in ptsB)
                    {
                        Point dsPt = srf.PointAtParameter(p.X, p.Y);
                        dsPtsB.Add(dsPt);
                        dsPts.Add(dsPt);
                    }

                    crvs.Add(NurbsCurve.ByPoints(dsPtsA));
                    crvs.Add(NurbsCurve.ByPoints(dsPtsB));

                    Surface panel = null;
                    try
                    {
                        panel = Surface.ByLoft(crvs);
                    }
                    catch (Exception ex) { }


                    m_points.Add(dsPts);
                    m_panels.Add(panel);
                }

            }
            return new Dictionary<string, object>
            {
                {"Panels", m_panels},
                {"Points", m_points },
            };
        }

        /// <summary>
        /// Create scewed quad panels
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <param name="t">Parameter for adjusted scewing amount (0-1)</param>
        /// <returns name = "QuadPanels">Quad Panel surfaces</returns>
        /// <returns name = "QuadPoints">Quad Panel points</returns>
        /// <returns name = "TriPanels">Tri Panel surfaces</returns>
        /// <returns name = "TriPoints">Tri Panel points</returns>
        [MultiReturn(new[] { "QuadPanels", "QuadPoints", "TriPanels", "TriPoints" })]
        public static Dictionary<string, object> PanelQuadScewed(Surface srf, int u, int v, double t)
        {
            List<List<Point>> m_quadpoints = new List<List<Point>>();
            List<Surface> m_quadpanels = new List<Surface>();

            List<List<Point>> m_tripoints = new List<List<Point>>();
            List<Surface> m_tripanels = new List<Surface>();

            TesselationUtils tess = new TesselationUtils();
            clsPanelCollection m_panelcollection = tess.PanelQuadScewed(u, v, t);
            foreach (clsQuad q in m_panelcollection.Quads)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));
                m_pts.Add(srf.PointAtParameter(q.D.X, q.D.Y));

                Surface panel = Surface.ByPerimeterPoints(m_pts);

                m_quadpoints.Add(m_pts);
                m_quadpanels.Add(panel);
            }

            foreach (clsTriangle q in m_panelcollection.Triangles)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));

                Surface panel = Surface.ByPerimeterPoints(m_pts);

                m_tripoints.Add(m_pts);
                m_tripanels.Add(panel);
            }
            return new Dictionary<string, object>
            {
                {"QuadPanels", m_quadpanels},
                {"QuadPoints", m_quadpoints },
                {"TriPanels", m_quadpanels},
                {"TriPoints", m_quadpoints },
            };
        }

        /// <summary>
        /// Create diamond panels
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <returns name = "QuadPanels">Quad Panel surfaces</returns>
        /// <returns name = "QuadPoints">Quad Panel points</returns>
        /// <returns name = "TriPanels">Tri Panel surfaces</returns>
        /// <returns name = "TriPoints">Tri Panel points</returns>
        [MultiReturn(new[] { "QuadPanels", "QuadPoints", "TriPanels", "TriPoints" })]
        public static Dictionary<string, object> PanelDiamond(Surface srf, int u, int v)
        {
            List<List<Point>> m_quadpoints = new List<List<Point>>();
            List<Surface> m_quadpanels = new List<Surface>();

            List<List<Point>> m_tripoints = new List<List<Point>>();
            List<Surface> m_tripanels = new List<Surface>();

            TesselationUtils tess = new TesselationUtils();
            clsPanelCollection m_panelcollection = tess.PanelDiamond(u, v);
            foreach (clsQuad q in m_panelcollection.Quads)
            {
                try
                {
                    List<Point> m_pts = new List<Point>();
                    m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                    m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                    m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));
                    m_pts.Add(srf.PointAtParameter(q.D.X, q.D.Y));

                    Surface panel = Surface.ByPerimeterPoints(m_pts);

                    m_quadpoints.Add(m_pts);
                    m_quadpanels.Add(panel);
                }
                catch { }

            }

            foreach (clsTriangle q in m_panelcollection.Triangles)
            {
                try
                {
                    List<Point> m_pts = new List<Point>();
                    m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                    m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                    m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));

                    Surface panel = Surface.ByPerimeterPoints(m_pts);

                    m_tripoints.Add(m_pts);
                    m_tripanels.Add(panel);
                }
                catch { }

            }
            return new Dictionary<string, object>
            {
                {"QuadPanels", m_quadpanels},
                {"QuadPoints", m_quadpoints },
                {"TriPanels", m_tripanels},
                {"TriPoints", m_tripoints },
            };
        }

        /// <summary>
        /// Create hexagonal panels
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <param name="t">Parameter for adjusted hexagons (0-1)</param>
        /// <returns name = "HexPanels">Hexagon Panel surfaces</returns>
        /// <returns name = "HexPoints">Hexagon Panel points</returns>
        /// <returns name = "PentPanels">Pentagon Panel surfaces</returns>
        /// <returns name = "PentPoints">Pentagon Panel points</returns>
        /// <returns name = "QuadPanels">Quad Panel surfaces</returns>
        /// <returns name = "QuadPoints">Quad Panel points</returns>
        [MultiReturn(new[] { "HexPanels", "HexPoints", "PentPanels", "PentPoints", "QuadPanels", "QuadPoints" })]
        public static Dictionary<string, object> PanelHexagon(Surface srf, int u, int v, double t)
        {
            List<List<Point>> m_quadpoints = new List<List<Point>>();
            List<Surface> m_quadpanels = new List<Surface>();

            List<List<Point>> m_pentpoints = new List<List<Point>>();
            List<Surface> m_pentpanels = new List<Surface>();

            List<List<Point>> m_hexpoints = new List<List<Point>>();
            List<Surface> m_hexpanels = new List<Surface>();

            TesselationUtils tess = new TesselationUtils();
            clsPanelCollection m_panelcollection = tess.PanelHexagonal(u, v, t);
            foreach (clsQuad q in m_panelcollection.Quads)
            {
                try
                {
                    List<Point> m_pts = new List<Point>();
                    m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                    m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                    m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));
                    m_pts.Add(srf.PointAtParameter(q.D.X, q.D.Y));

                    Surface panel = Surface.ByPerimeterPoints(m_pts);

                    m_quadpoints.Add(m_pts);
                    m_quadpanels.Add(panel);
                }
                catch { }

            }

            foreach (clsPentagon p in m_panelcollection.Pentagons)
            {
                try
                {
                    List<Point> m_pts = new List<Point>();
                    m_pts.Add(srf.PointAtParameter(p.A.X, p.A.Y));
                    m_pts.Add(srf.PointAtParameter(p.B.X, p.B.Y));
                    m_pts.Add(srf.PointAtParameter(p.C.X, p.C.Y));
                    m_pts.Add(srf.PointAtParameter(p.D.X, p.D.Y));
                    m_pts.Add(srf.PointAtParameter(p.E.X, p.E.Y));

                    Surface panel = Surface.ByPerimeterPoints(m_pts);

                    m_pentpoints.Add(m_pts);
                    m_pentpanels.Add(panel);
                }
                catch { }

            }

            foreach (clsHexagon p in m_panelcollection.Hexagons)
            {
                try
                {
                    List<Point> m_pts = new List<Point>();
                    m_pts.Add(srf.PointAtParameter(p.A.X, p.A.Y));
                    m_pts.Add(srf.PointAtParameter(p.B.X, p.B.Y));
                    m_pts.Add(srf.PointAtParameter(p.C.X, p.C.Y));
                    m_pts.Add(srf.PointAtParameter(p.D.X, p.D.Y));
                    m_pts.Add(srf.PointAtParameter(p.E.X, p.E.Y));
                    m_pts.Add(srf.PointAtParameter(p.F.X, p.F.Y));

                    Surface panel = Surface.ByPerimeterPoints(m_pts);

                    m_hexpoints.Add(m_pts);
                    m_hexpanels.Add(panel);
                }
                catch { }

            }

            return new Dictionary<string, object>
            {
                {"QuadPanels", m_quadpanels},
                {"QuadPoints", m_quadpoints },
                {"PentPanels", m_pentpanels},
                {"PentPoints", m_pentpoints },
                {"HexPanels", m_hexpanels},
                {"HexPoints", m_hexpoints },
            };
        }

        /// <summary>
        /// Create Triangle A panels
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <returns name = "Panels">Panel surfaces</returns>
        /// <returns name = "Points">Panel points</returns>
        [MultiReturn(new[] { "Panels", "Points" })]
        public static Dictionary<string, object> PanelTriangleA(Surface srf, int u, int v)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Surface> m_panels = new List<Surface>();

            TesselationUtils tess = new TesselationUtils();
            List<clsTriangle> m_tris = tess.PanelTriangleA(u, v);
            foreach (clsTriangle q in m_tris)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));

                Surface panel = Surface.ByPerimeterPoints(m_pts);

                m_points.Add(m_pts);
                m_panels.Add(panel);
            }
            return new Dictionary<string, object>
            {
                {"Panels", m_panels},
                {"Points", m_points },
            };
        }

        /// <summary>
        /// Create Triangle B panels
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <returns name = "Panels">Panel surfaces</returns>
        /// <returns name = "Points">Panel points</returns>
        [MultiReturn(new[] { "Panels", "Points" })]
        public static Dictionary<string, object> PanelTriangleB(Surface srf, int u, int v)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Surface> m_panels = new List<Surface>();

            TesselationUtils tess = new TesselationUtils();
            List<clsTriangle> m_tris = tess.PanelTriangleB(u, v);
            foreach (clsTriangle q in m_tris)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));

                Surface panel = Surface.ByPerimeterPoints(m_pts);

                m_points.Add(m_pts);
                m_panels.Add(panel);
            }
            return new Dictionary<string, object>
            {
                {"Panels", m_panels},
                {"Points", m_points },
            };
        }

        /// <summary>
        /// Create Triangle C panels
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <returns name = "Panels">Panel surfaces</returns>
        /// <returns name = "Points">Panel points</returns>
        [MultiReturn(new[] { "Panels", "Points" })]
        public static Dictionary<string, object> PanelTriangleC(Surface srf, int u, int v)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Surface> m_panels = new List<Surface>();

            TesselationUtils tess = new TesselationUtils();
            List<clsTriangle> m_tris = tess.PanelTriangleC(u, v);
            foreach (clsTriangle q in m_tris)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(q.A.X, q.A.Y));
                m_pts.Add(srf.PointAtParameter(q.B.X, q.B.Y));
                m_pts.Add(srf.PointAtParameter(q.C.X, q.C.Y));

                Surface panel = Surface.ByPerimeterPoints(m_pts);

                m_points.Add(m_pts);
                m_panels.Add(panel);
            }
            return new Dictionary<string, object>
            {
                {"Panels", m_panels},
                {"Points", m_points },
            };
        }
    }

    /// <summary>
    /// Structure Systems
    /// </summary>
    public static class Structure
    {
        /// <summary>
        /// Create wireframe grid
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <returns name = "Lines">Wireframe lines</returns>
        /// <returns name = "Points">Wireframe points</returns>
        [MultiReturn(new[] { "Lines", "Points" })]
        public static Dictionary<string, object> WireGrid(Surface srf, int u, int v)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Line> m_lines = new List<Line>();

            TesselationUtils tess = new TesselationUtils();
            List<clsLine> m_quads = tess.WireGrid(u, v);
            foreach (clsLine l in m_quads)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(l.A.X, l.A.Y));
                m_pts.Add(srf.PointAtParameter(l.B.X, l.B.Y));

                Line crv = Line.ByBestFitThroughPoints(m_pts);

                m_points.Add(m_pts);
                m_lines.Add(crv);
            }
            return new Dictionary<string, object>
            {
                {"Lines", m_lines},
                {"Points", m_points },
            };
        }

        /// <summary>
        /// Create wireframe grid braced in a 1 direction
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <returns name = "Lines">Wireframe lines</returns>
        /// <returns name = "Points">Wireframe points</returns>
        [MultiReturn(new[] { "Lines", "Points" })]
        public static Dictionary<string, object> WireGridBraced1D(Surface srf, int u, int v)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Line> m_lines = new List<Line>();

            TesselationUtils tess = new TesselationUtils();
            List<clsLine> m_quads = tess.WireBraced1DGrid(u, v);
            foreach (clsLine l in m_quads)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(l.A.X, l.A.Y));
                m_pts.Add(srf.PointAtParameter(l.B.X, l.B.Y));

                Line crv = Line.ByBestFitThroughPoints(m_pts);

                m_points.Add(m_pts);
                m_lines.Add(crv);
            }
            return new Dictionary<string, object>
            {
                {"Lines", m_lines},
                {"Points", m_points },
            };
        }

        /// <summary>
        /// Create wireframe grid braced in a 2 directions
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <returns name = "Lines">Wireframe lines</returns>
        /// <returns name = "Points">Wireframe points</returns>
        [MultiReturn(new[] { "Lines", "Points" })]
        public static Dictionary<string, object> WireGridBraced2D(Surface srf, int u, int v)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Line> m_lines = new List<Line>();

            TesselationUtils tess = new TesselationUtils();
            List<clsLine> m_quads = tess.WireBraced2DGrid(u, v);
            foreach (clsLine l in m_quads)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(l.A.X, l.A.Y));
                m_pts.Add(srf.PointAtParameter(l.B.X, l.B.Y));

                Line crv = Line.ByBestFitThroughPoints(m_pts);

                m_points.Add(m_pts);
                m_lines.Add(crv);
            }
            return new Dictionary<string, object>
            {
                {"Lines", m_lines},
                {"Points", m_points },
            };
        }

        /// <summary>
        /// Create wireframe diagrid
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <param name="t">Diagrid type switch.</param>
        /// <returns name = "Lines">Wireframe lines</returns>
        /// <returns name = "Points">Wireframe points</returns>
        [MultiReturn(new[] { "Lines", "Points" })]
        public static Dictionary<string, object> WireDiagrid(Surface srf, int u, int v, bool t)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Line> m_lines = new List<Line>();

            TesselationUtils tess = new TesselationUtils();
            List<clsLine> m_quads = tess.WireDiagrid(u, v, t);
            foreach (clsLine l in m_quads)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(l.A.X, l.A.Y));
                m_pts.Add(srf.PointAtParameter(l.B.X, l.B.Y));

                Line crv = Line.ByBestFitThroughPoints(m_pts);

                m_points.Add(m_pts);
                m_lines.Add(crv);
            }
            return new Dictionary<string, object>
            {
                {"Lines", m_lines},
                {"Points", m_points },
            };
        }

        /// <summary>
        /// Create wireframe hexagon
        /// </summary>
        /// <param name="srf">Surface</param>
        /// <param name="u">U Divisions</param>
        /// <param name="v">V Divisions</param>
        /// <param name="a">Hexagon shape adjustment</param>
        /// <param name="t">Hexagon type switch</param>
        /// <returns name = "Lines">Wireframe lines</returns>
        /// <returns name = "Points">Wireframe points</returns>
        [MultiReturn(new[] { "Lines", "Points" })]
        public static Dictionary<string, object> WireHexagon(Surface srf, int u, int v, double a, bool t)
        {
            List<List<Point>> m_points = new List<List<Point>>();
            List<Line> m_lines = new List<Line>();

            TesselationUtils tess = new TesselationUtils();
            List<clsLine> m_quads = tess.WireHexagon(u, v, a, t);
            foreach (clsLine l in m_quads)
            {
                List<Point> m_pts = new List<Point>();
                m_pts.Add(srf.PointAtParameter(l.A.X, l.A.Y));
                m_pts.Add(srf.PointAtParameter(l.B.X, l.B.Y));

                Line crv = Line.ByBestFitThroughPoints(m_pts);

                m_points.Add(m_pts);
                m_lines.Add(crv);
            }
            return new Dictionary<string, object>
            {
                {"Lines", m_lines},
                {"Points", m_points },
            };
        }
    }

    /// <summary>
    /// Vectors
    /// </summary>
    public static class Vectors
    {

    }
}
