﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LunchBox for Dynamo")]
[assembly: AssemblyDescription("Tools for computational design and productivity with Dynamo")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("PROVING GROUND")]
[assembly: AssemblyProduct("LunchBox")]
[assembly: AssemblyCopyright("Copyright ©  2020 PROVING GROUND LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("720afdd7-46fe-48fc-907f-aa60e7664da4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2017.8.7.0")]
[assembly: AssemblyFileVersion("2017.8.7.0")]
